cpan-listchanges (0.08-2) unstable; urgency=medium

  [ Xavier Guimard ]
  * Email change: Xavier Guimard -> yadd@debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Submit.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata (already present in machine-readable debian/copyright).
  * Update standards version to 4.2.1, no changes needed.
  * Bump debhelper from old 12 to 13.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 09 Dec 2022 16:15:24 +0000

cpan-listchanges (0.08-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Jonathan Yu from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * Import upstream version 0.08.
  * Declare compliance with Debian Policy 4.2.0.
  * Drop ancient alternative (build) dependency.
  * Bump debhelper compatibility level to 10.

 -- gregor herrmann <gregoa@debian.org>  Tue, 21 Aug 2018 21:03:35 +0200

cpan-listchanges (0.07-1) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * Rename autopkgtest configuration file(s) as per new pkg-perl-
    autopkgtest schema.

  [ Lucas Kanashiro ]
  * Import upstream version 0.07
  * Bump debhelper compatibility level to 9
  * Update year of upstream copyright

 -- Lucas Kanashiro <kanashiro.duarte@gmail.com>  Thu, 19 Nov 2015 15:34:34 -0200

cpan-listchanges (0.06-2) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * debian/control: update Module::Build dependency.
  * Add debian/upstream/metadata
  * Declare compliance with Debian Policy 3.9.6.
  * Drop version from libmodule-build-perl build dependency.
  * Mark package as autopkgtest-able.
  * Move libmodule-build-perl to Build-Depends.

 -- gregor herrmann <gregoa@debian.org>  Mon, 01 Jun 2015 22:56:20 +0200

cpan-listchanges (0.06-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Xavier Guimard ]
  * Imported Upstream version 0.06
  * Update debian/copyright:
    + bump format to 1.0
    + update years
    + remove Module::Install license not included in upstream now
  * Bump Standards-Version to 3.9.4
  * Add libmodule-build-perl in build dependencies

 -- Xavier Guimard <x.guimard@free.fr>  Sun, 16 Jun 2013 07:56:07 +0200

cpan-listchanges (0.05-1) unstable; urgency=low

  * Initial Release (Closes: #617788)

 -- Jonathan Yu <jawnsy@cpan.org>  Fri, 11 Mar 2011 20:07:50 -0500
